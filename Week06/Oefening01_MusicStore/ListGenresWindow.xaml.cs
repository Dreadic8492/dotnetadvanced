﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Oefening01_MusicStore
{
    /// <summary>
    /// Interaction logic for ListGenresWindow.xaml
    /// </summary>
    public partial class ListGenresWindow : Window
    {
        public ListGenresWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Oefening01_MusicStore.MvcMusicStoreDataSet mvcMusicStoreDataSet = ((Oefening01_MusicStore.MvcMusicStoreDataSet)(this.FindResource("mvcMusicStoreDataSet")));
            // Load data into the table Genre. You can modify this code as needed.
            Oefening01_MusicStore.MvcMusicStoreDataSetTableAdapters.GenreTableAdapter mvcMusicStoreDataSetGenreTableAdapter = new Oefening01_MusicStore.MvcMusicStoreDataSetTableAdapters.GenreTableAdapter();
            mvcMusicStoreDataSetGenreTableAdapter.Fill(mvcMusicStoreDataSet.Genre);
            System.Windows.Data.CollectionViewSource genreViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("genreViewSource")));
            genreViewSource.View.MoveCurrentToFirst();
        }
    }
}
