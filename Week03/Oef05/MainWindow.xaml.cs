﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oef05
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void calcButton_Click(object sender, RoutedEventArgs e)
        {
            Double output = 0.0;
            bool result = Double.TryParse(lengteTextBox.Text, out output);
            if (result)
            {
                resultLabel.Content = String.Format("{0:0.00} knopen", Math.Sqrt(output) * 1.34);
            }
            else
            {
                MessageBox.Show("Geen geldige bootlengte ingegeven!");
            }

        }
    }
}
