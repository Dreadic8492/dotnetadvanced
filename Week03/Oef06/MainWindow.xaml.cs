﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oef06
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            frame.NavigationService.Navigate(new Page1());
        }

        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;
            switch (tempButton.Name)
            {
                case "goNextButton":
                    GoNext();
                    break;
                case "goBackButton":
                    GoBack();
                    break;
            }
        }

        private void GoNext()
        {
            frame.NavigationService.Navigate(new Page2());

        }

        private void GoBack()
        {
            if (frame.NavigationService.CanGoBack)
            {
                frame.NavigationService.Navigate(new Page1());
            }
        }
    }
}