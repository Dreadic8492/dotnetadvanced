﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oef07
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextChangedHandler(object sender, TextChangedEventArgs e)
        {
            bool result = false;
            String text = ((TextBox)sender).Text.ToString();
            foreach (char c in text)
            {
                if (Char.IsNumber(c))
                {
                    result = true;
                }
            }
            if (result)
            {
                MessageBox.Show("Enkel karakters mogen ingegeven worden!");
            }
        }

    }
}
