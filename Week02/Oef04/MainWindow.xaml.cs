﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oef04
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox tempCheckBox = (CheckBox)sender;
            switch (tempCheckBox.Name)
            {
                case "manCheckBox":
                    manCheckBox.IsChecked = true;
                    vrouwCheckBox.IsChecked = false;
                    break;
                case "vrouwCheckBox":
                    vrouwCheckBox.IsChecked = true;
                    manCheckBox.IsChecked = false;
                    break;
            }
        }
    }
}
