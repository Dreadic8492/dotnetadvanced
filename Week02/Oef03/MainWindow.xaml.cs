﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace Oef03
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Shrink()
        {
            if (rectangle.Width > 10 && rectangle.Height > 10)
            {
                rectangle.Width -= 10;
                rectangle.Height -= 10; 
            }            
        }

        private void Grow()
        {
            if (rectangle.Width < this.Width && rectangle.Height < this.Height)
            {
                rectangle.Width += 10;
                rectangle.Height += 10;
            }            
        }

        private void MouseClickHandler(object sender, RoutedEventArgs e)
        {
            RepeatButton tempButton = (RepeatButton)sender;
            switch (tempButton.Name)
            {
                case "growButton":
                    Grow();
                    break;
                case "shrinkButton":
                    Shrink();
                    break;
            }
        }



    }
}
