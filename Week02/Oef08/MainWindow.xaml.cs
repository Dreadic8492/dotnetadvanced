﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oef08
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            calculateBMI();
        }

        private void determineStatus(double bmi)
        {
            String status = String.Empty;
            if (bmi < 18.5)
            {
                status = "Ondergewicht";
                statusLabel.Background = new SolidColorBrush(Colors.Orange);
            }
            else if (bmi < 25)
            {
                status = "Normaal gewicht";
                statusLabel.Background = new SolidColorBrush(Colors.Green);
            }
            else if (bmi < 30)
            {
                status = "Overgewicht";
                statusLabel.Background = new SolidColorBrush(Colors.Orange);
            }
            else if (bmi >= 30)
            {
                status = "Obesitas";
                statusLabel.Background = new SolidColorBrush(Colors.Red);
            }
            statusLabel.Content = status;
        }

        private void calculateBMI()
        {
            //BMI = ( Weight in Kilograms / ( Height in Meters x Height in Meters ) )
            if (!(gewichtSlider == null || lengteSlider == null || bmiLabel == null))
            {
                Double gewicht = gewichtSlider.Value;
                Double lengte = lengteSlider.Value / 100.0;
                Double bmi = gewicht / (lengte * lengte);
                bmiLabel.Content = Math.Round(bmi,2);
                determineStatus(bmi);
            }
            
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            calculateBMI();
        }
    }
}
