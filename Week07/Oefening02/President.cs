﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Oefening02
{
    class President
    {

        public String Name { get; set;}
        public String PhotoSource { get; set; }

        public President(String naam, String photoSource)
        {
            this.Name = naam;
            this.PhotoSource = photoSource;
        }
    }
}
