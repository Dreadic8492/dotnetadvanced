﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening02
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            List<President> presidentsList = new List<President>();
            presidentsList.Add(new President("George Washington", "/presidents/gw1.jpg"));
            presidentsList.Add(new President("John Adams", "/presidents/ja2.jpg"));
            presidentsList.Add(new President("Thomas Jefferson","/presidents/tj3.jpg"));
            presidentsList.Add(new President("James Madison", "/presidents/jm4.jpg"));
            presidentsList.Add(new President("James Monroe", "/presidents/jm5.jpg"));
            presidentsList.Add(new President("John Quincy Adams", "/presidents/ja6.jpg"));
            presidentsList.Add(new President("Andrew Jackson", "/presidents/aj7.jpg"));
            presidentsList.Add(new President("Martin Van Buren", "/presidents/mv8.jpg"));
            presidentsList.Add(new President("William H. Harrison", "/presidents/wh9.jpg"));
            presidentsList.Add(new President("John Tyler", "/presidents/jt10.jpg"));
            presidentsList.Add(new President("James K. Pol", "/presidents/jp11.jpg"));
            presidentsList.Add(new President("Zachary Taylor", "/presidents/zt12.jpg"));
            presidentsList.Add(new President("Millard Fillmore", "/presidents/mf13.jpg"));
            presidentsList.Add(new President("Franklin Pierce", "/presidents/fp14.jpg"));
            presidentsList.Add(new President("James Buchanan", "/presidents/jp15.jpg"));
            presidentsList.Add(new President("Abraham Lincoln", "/presidents/al16.jpg"));
            presidentsList.Add(new President("Andrew Johnson", "/presidents/aj17.jpg"));
            presidentsList.Add(new President("Ulysses S. Grant", "/presidents/ug18.jpg"));
            presidentsList.Add(new President("Rutherford B. Hayes", "/presidents/rb19.jpg"));
            presidentsList.Add(new President("James Garfield", "/presidents/jg20.jpg"));
            presidentsListBox.DataContext = presidentsList;
            presidentsListBox.SelectedIndex = 0;
            
        }

        private void presidentsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            President temp = (President)presidentsListBox.SelectedItem;
            this.DataContext = temp;
        }
    }
}
