﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Oefening03
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Person person;
        public MainWindow()
        {
            InitializeComponent();
            person = new Person("<Enter First Name>", "<Enter Name>");
            DataContext = person;
            UpdateFullNameLabel();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            TextBox tempTextBox = (TextBox)sender;
            switch (tempTextBox.Name)
            {
                case "firstNameTextBox":
                    person.Name = nameTextBox.Text;
                    break;
                case "NameTextBox":
                    person.FirstName = firstNameTextBox.Text;
                    break;
            }
            UpdateFullNameLabel();
        }

        private void UpdateFullNameLabel()
        {
            fullNameLabel.Content = person.ToString();
        }
    }
}
