﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Oefening03
{
    class Person : INotifyPropertyChanged
    {
        private String name;
        private String firstName;
        public String Name {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
                OnPropertyChanged("Name");
            }
        }
        public String FirstName {
            get
            {
                return this.firstName;
            }
            set
            {
                this.firstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public Person(String name, String firstName)
        {
            this.name = name;
            this.firstName = firstName;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", firstName, name);
        }
    }
}
