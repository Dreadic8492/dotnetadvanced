﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Voorbeeld_Collection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ObservableCollection<Employee> employeeList = new ObservableCollection<Employee>();
            employeeList.Add(new Employee("Hans Habraken", "Developer"));
            employeeList.Add(new Employee("Brian Poncelet", "Developer"));
            employeeList.Add(new Employee("Robin Lycops", "Developer"));
            employeeList.Add(new Employee("Frankie Claessens", "CEO"));
            DataContext = employeeList;
        }
    }
}
