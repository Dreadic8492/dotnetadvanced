﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Oefening01
{
    class Student : INotifyPropertyChanged
    {
        private String naam;
        public String Naam {
            get
            {
                return naam;
            }
            set
            {
                naam = value;
                OnPropertyChanged("Naan");
            }
        }
        private String straat;
        public String Straat
        {
            get
            {
                return straat;
            }
            set
            {
                straat = value;
                OnPropertyChanged("Straat");
            }
        }

        private String stad;
        public String Stad
        {
            get
            {
                return stad;
            }
            set
            {
                stad = value;
                OnPropertyChanged("Stad");
            }
        }

        private String provincie;
        public String Provincie
        {
            get
            {
                return provincie;
            }
            set
            {
                provincie = value;
                OnPropertyChanged("Provincie");
            }
        }

        private String postcode;
        public String Postcode
        {
            get
            {
                return postcode;
            }
            set
            {
                postcode = value;
                OnPropertyChanged("Postcode");
            }
        }

        private int telefoonnummer;
        public int Telefoonnummer
        {
            get
            {
                return telefoonnummer;
            }
            set
            {
                telefoonnummer = value;
                OnPropertyChanged("Telefoonnummer");
            }
        }

        public List<String> ProvincieList = new List<String> { "Antwerpen", "Limburg", "Oost Vlaanderen", "Vlaams Brabant", "West Vlaanderen" };

        public Student(String naam, String straat, String stad, String provincie, String postcode, int telefoonnummer)
        {
            this.naam = naam;
            this.straat = straat;
            this.stad = stad;
            this.provincie = provincie;
            this.postcode = postcode;
            this.telefoonnummer = telefoonnummer;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] string caller = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(caller));
            }
        }
    }
}
