﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace Oefening01
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ObservableCollection<Student> studentenList = new ObservableCollection<Student>();
            studentenList.Add(new Student("Frankie Claessens", "Whateverstraat", "Genk", "Limburg", "3600", 031959595));
            studentenList.Add(new Student("Hans Habraken", "WaMakeStraat 27", "Brussel", "Vlaams Brabant", "1100", 011999999));
            studentenList.Add(new Student("Hannah Patronoudis", "Geenideestraat 99", "Genk", "Limburg", "3600", 089303030));
            studentenList.Add(new Student("Brecht Philips", "Beuhstraat 99", "Antwerpen", "Antwerpen", "2000", 099353535));
            studentsListBox.DataContext = studentenList;
            studentsListBox.SelectedIndex = 0;
        }

        private void studentsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Student temp = (Student)studentsListBox.SelectedItem;
            studentDetails.DataContext = temp;
            provincieComboBox.DataContext = temp.ProvincieList;
            provincieComboBox.SelectedItem = temp.Provincie;
        }
    }
}
