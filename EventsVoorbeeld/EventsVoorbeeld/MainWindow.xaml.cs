﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace EventsVoorbeeld
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            CommandBinding copyCommand = new CommandBinding(ApplicationCommands.Copy);
            CommandBinding pasteCommand = new CommandBinding(ApplicationCommands.Paste);

            copyButton.CommandBindings.Add(copyCommand);
            pasteButton.CommandBindings.Add(pasteCommand);

            copyCommand.Executed += copyCommand_Executed;
            pasteCommand.Executed += pasteCommand_Executed;
//          copyCommand.CanExecute += copyCommand_CanExecute;
//          pasteCommand.CanExecute += pasteCommand_CanExecute;
        }

        void pasteCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
//           e.CanExecute = false;
        }

        void pasteCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Debug.WriteLine("Paste Command Executed");
        }

        void copyCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
//           e.CanExecute = false;
        }

        void copyCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Debug.WriteLine("Copy Command Executed");
        }
    }
}
