﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Terms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CollectionViewSource termsViewSource;
        private Terms.PayablesDataSetTableAdapters.TermsTableAdapter payablesDataSetTermsTableAdapter;
        private Terms.PayablesDataSet payablesDataSet;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            // Load data into the table Terms. You can modify this code as needed.
            payablesDataSetTermsTableAdapter = new Terms.PayablesDataSetTableAdapters.TermsTableAdapter();
            payablesDataSet = ((Terms.PayablesDataSet)(this.FindResource("payablesDataSet")));
            payablesDataSetTermsTableAdapter.Fill(payablesDataSet.Terms);
            termsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("termsViewSource")));
            termsViewSource.View.MoveCurrentToFirst();
        }

        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;
            switch (tempButton.Name)
            {
                case "firstButton":
                    termsViewSource.View.MoveCurrentToFirst();
                    UpdatePositionStatus();
                    break;
                case "previousButton":
                    if (termsViewSource.View.CurrentPosition>0)
                    {
                        termsViewSource.View.MoveCurrentToPrevious();
                        UpdatePositionStatus();
                    }

                    break;
                case "nextButton":
                    if (termsViewSource.View.CurrentPosition<((CollectionView)termsViewSource.View).Count - 1)
                    {
                        termsViewSource.View.MoveCurrentToNext();
                        UpdatePositionStatus();
                    }                   
                    break;
                case "lastButton":
                    termsViewSource.View.MoveCurrentToLast();
                    UpdatePositionStatus();
                    break;
            }
        }

        private void UpdatePositionStatus()
        {
            positionTextbox.Text = String.Format("{0} of {1}", termsViewSource.View.CurrentPosition + 1, ((CollectionView)termsViewSource.View).Count);
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            payablesDataSetTermsTableAdapter.Update(payablesDataSet.Terms);
        }
    }
}
