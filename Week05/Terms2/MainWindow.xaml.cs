﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;

namespace Terms2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Terms2.PayablesDataSetTableAdapters.TermsTableAdapter payablesDataSetTermsTableAdapter;
        Terms2.PayablesDataSet payablesDataSet;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            payablesDataSet = ((Terms2.PayablesDataSet)(this.FindResource("payablesDataSet")));
            // Load data into the table Terms. You can modify this code as needed.
            payablesDataSetTermsTableAdapter = new Terms2.PayablesDataSetTableAdapters.TermsTableAdapter();
             try
            {
                payablesDataSetTermsTableAdapter.Fill(payablesDataSet.Terms);   
            }
            catch (SqlException ex)
            {
                //Error geven als Database niet bestaat -> App.config -> Initial Catalog van naam veranderen om te testen!
                MessageBox.Show(String.Format("SQL Server error:\n\r\n\r#{0}\n\r\n\rMessage:{1}\n\r\n\rType:{2}",ex.Number,ex.Message,ex.GetType().ToString()));
            }
           
            System.Windows.Data.CollectionViewSource termsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("termsViewSource")));
            termsViewSource.View.MoveCurrentToFirst();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //Geeft alle records groter dan number terug (dus de waarde in de textbox)
            int number;
            bool result = Int32.TryParse(termsTextBox.Text, out number);
            if (result)
            {
                payablesDataSetTermsTableAdapter.FillByTerms(payablesDataSet.Terms, number);
            }
            else
            {
                MessageBox.Show("Geen getal ingegeven!");
            }
        }
    }
}
