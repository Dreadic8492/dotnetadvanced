﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
namespace Voorbeeld
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            Voorbeeld.PayablesDataSet payablesDataSet = ((Voorbeeld.PayablesDataSet)(this.FindResource("payablesDataSet")));
            // Load data into the table Terms. You can modify this code as needed.
            Voorbeeld.PayablesDataSetTableAdapters.TermsTableAdapter payablesDataSetTermsTableAdapter = new Voorbeeld.PayablesDataSetTableAdapters.TermsTableAdapter();
            payablesDataSetTermsTableAdapter.Fill(payablesDataSet.Terms);
            System.Windows.Data.CollectionViewSource termsViewSource = ((System.Windows.Data.CollectionViewSource)(this.FindResource("termsViewSource")));
            termsViewSource.View.MoveCurrentToFirst();
        }
    }
}
